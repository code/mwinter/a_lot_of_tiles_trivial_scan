\version "2.19.81"

% systems-per-page, unfold vars, and strings delimited by tildes 
% are changed by the Supercollider transcriber

\paper {
  #(set-paper-size "a4" 'portrait)
  top-margin = 1 \cm
  bottom-margin = 1 \cm
  left-margin = 2.5 \cm
  
  top-system-spacing =
  #'((basic-distance . 25 )
  (minimum-distance . 25 )
  (padding . 0 )
  (stretchability . 0))
  
  last-bottom-spacing =
  #'((basic-distance . 15 )
  (minimum-distance . 15 )
  (padding . 0 )
  (stretchability . 0))
  
  % manually change systems-per-page to 2 if the harmonics of hl 6 are uncommented below
  systems-per-page = 8
  
  print-page-number = ##t
  oddHeaderMarkup = \markup { \on-the-fly #not-first-page "(part - hl 6 - 2101000)" }
  evenHeaderMarkup = \markup { \on-the-fly #not-first-page "(part - hl 6 - 2101000)" }
  oddFooterMarkup = \markup { \fill-line {
    \on-the-fly #not-first-page 
    \concat {
      "-"
      \fontsize #1.5
      \on-the-fly #print-page-number-check-first
      \fromproperty #'page:page-number-string
      "-"}}}
  evenFooterMarkup = \markup { \fill-line {
    \on-the-fly #not-first-page 
    \concat { 
      "-" 
      \fontsize #1.5
      \on-the-fly #print-page-number-check-first
      \fromproperty #'page:page-number-string
      "-"}}}
}

\header {
  title = \markup { \italic {a lot of tiles (trivial scan)}}
  subtitle = \markup { \normal-text {2101000}}
  composer = \markup \left-column {"michael winter" "(cdmx, mx and nashville, usa; 2018)"}
  piece = \markup { \normal-text {part - hl 6 - 2101000}}
  tagline = ""
} 

#(set-global-staff-size 15)

\layout {
  indent = 0.0\cm
  line-width = 17\cm 
  ragged-last = ##t
  
  \context {
    \Score
      \override BarNumber.extra-offset = #'(0 . 5)
      \override BarNumber.stencil = 
      #(make-stencil-circler 0.1 0.25 ly:text-interface::print)
  }
  \context {
    \Staff
      \override StaffSymbol.line-count = #1
      \override Clef.stencil = #point-stencil 
      \override Clef.space-alist.first-note = #'(extra-space . 1)
      \remove "Time_signature_engraver"
  }
  \context {
    \Voice
      \override Glissando.minimum-length = #0
      \override Glissando.layer = 500
      \override Glissando.breakable = ##t
      \override Glissando.bound-details =
      #'((right
          (attach-dir . -1)
          (end-on-accidental . #f)
          (padding . 0))
         (right-broken
          (padding . 0.5))
         (left-broken
          (padding . 0.5)
          (attach-dir . 1))
         (left
          (attach-dir . -1)
          (padding . -0.25)
          (start-at-dot . #f)))
    }
}

% this draws a curve and a gradient
#(define (make-grey-filled-curve-stencil-list x-coords colors half-thick rl)
  (if (null? (cdr x-coords))
      rl
      (make-grey-filled-curve-stencil-list
        (cdr x-coords)
        (cdr colors)
        half-thick
        (cons
          (stencil-with-color
            (make-filled-box-stencil
              (interval-widen (cons (car x-coords) (cadr x-coords)) 0.1)
              (cons (- half-thick) (- 9 (* 10 (car colors)))))
            (list (car colors) (car colors) (car colors)))
          rl))))

% this draws a just a line with a gradient
#(define (make-grey-filled-box-stencil-list x-coords colors half-thick rl)
  (if (null? (cdr x-coords))
      rl
      (make-grey-filled-box-stencil-list
        (cdr x-coords)
        (cdr colors)
        half-thick
        (cons
          (stencil-with-color
            (make-filled-box-stencil
              (interval-widen (cons (car x-coords) (cadr x-coords)) 0.1)
              (cons (- half-thick) half-thick))
            (list (car colors) (car colors) (car colors)))
          rl))))

#(define my-gliss
  (lambda (grob)
    (if (ly:stencil? (ly:line-spanner::print grob))
        (let* ((stencil (ly:line-spanner::print grob))
               (X-ext (ly:stencil-extent stencil X))
               (Y-ext (ly:stencil-extent stencil Y))
               (Y-length (- (cdr Y-ext) (car Y-ext)))
               (left-bound-info (ly:grob-property grob 'left-bound-info))
               (left-Y (assoc-get 'Y left-bound-info))
               (thick
                 (assoc-get 'thickness (ly:grob-property grob 'details) 0.5))
               (layout (ly:grob-layout grob))
               (blot (ly:output-def-lookup layout 'blot-diameter))
               (right-bound (ly:spanner-bound grob RIGHT))
               (right-par (ly:grob-parent right-bound X))
               (stem
                 (if (grob::has-interface right-par 'note-column-interface)
                     (ly:grob-object right-par 'stem)
                     '()))
               (stem-stencil
                 (if (ly:grob? stem)
                     (ly:grob-property stem 'stencil)
                     #f))
               (stem-thick
                 (if (ly:stencil? stem-stencil)
                     (interval-length (ly:stencil-extent stem-stencil X))
                     0))
               (corr-delta-X (- (interval-length X-ext)
                    Y-length
                    blot
                    stem-thick
                    ;; mmh, why this value??
                    -0.05))
               (colors
                 (assoc-get 'colors (ly:grob-property grob 'details) 
                   (list 0 .5 .8)))
               (steps
                 (length colors))
               (raw-x-coords
                 (iota (1+ (abs steps)) 0 (/ corr-delta-X (abs steps))))
               (x-coords
                 (map
                   (lambda (e)
                     (+ (car X-ext) Y-length blot e))
                   raw-x-coords)))

          ;; create a flat glissando
          (ly:grob-set-nested-property! grob '(right-bound-info Y) left-Y)
          
          ;; return the stencil of added boxes
          (ly:stencil-translate-axis
             (apply
               ly:stencil-add
               ;; change this to make-grey-filled-box-stencil-list to eleminate curve and just use gradient
               (make-grey-filled-curve-stencil-list
                 x-coords
                 colors
                 thick
                 '()))
            ;; the actual offset is TODO, hardcoded here
              3.5
            Y))
         #f)))

#(define (add-gliss m)
   (case (ly:music-property m 'name)
     ((NoteEvent)
      (set! (ly:music-property m 'articulations)
            (append
              (ly:music-property m 'articulations)
              (list (make-music 'GlissandoEvent))))
      m)
     (else #f)))

addGliss =
#(define-music-function (music)
  (ly:music?)
  (map-some-music add-gliss music))


\new Score 
\with {proportionalNotationDuration = #(ly:make-moment 1 16)}
<<
  \new StaffGroup \with{
    \override StaffGrouper.staff-staff-spacing =
    #'((basic-distance . 13) 
       (minimum-distance . 13)
       (padding . 0)
       (stretchability . 0))
    \override StaffGrouper.staffgroup-staff-spacing = 
    #'((basic-distance . 13) 
       (minimum-distance . 13)
       (padding . 0)
       (stretchability . 0))
  }
  <<
    
    % uncomment these lines to view harmonics of hl 6
    % also change systems-per-page to 2 manually above
    %{
    \new Staff \with {
      instrumentName = \markup \center-column {"hl 6 " "(harm 13)  "}
      shortInstrumentName = #"hl6 (h13)  "
    }
    <<
      \repeat unfold 33 { \repeat unfold 3 { s1 \noBreak } s1 \break }
      \include "includes/a_lot_of_tiles_trivial_scan_hl_6_harm_13.ly"
    >>
    \new Staff \with {
      instrumentName = \markup \center-column {"hl 6 " "(harm 9)   "}
      shortInstrumentName = #"hl6 (h9)  "
    } 
    <<
      \repeat unfold 33 { \repeat unfold 3 { s1 \noBreak } s1 \break }
      \include "includes/a_lot_of_tiles_trivial_scan_hl_6_harm_9.ly"
    >>
    \new Staff \with {
      instrumentName = \markup \center-column {"hl 6 " "(harm 5)   "}
      shortInstrumentName = #"hl6 (h5)  "
    } 
    <<
      \repeat unfold 33 { \repeat unfold 3 { s1 \noBreak } s1 \break }
      \include "includes/a_lot_of_tiles_trivial_scan_hl_6_harm_5.ly"
    >>
    %}
    
     %~~hl6-start~~
    \new Staff \with {
      instrumentName = \markup \center-column {"hl 6  " "(harm 1)   "}
      shortInstrumentName = #"hl6 (h1)  "
    }
    <<
      \repeat unfold 33 { \repeat unfold 3 { s1 \noBreak } s1 \break }
      \include "includes/a_lot_of_tiles_trivial_scan_hl_6_harm_1.ly"
    >>
    %~~hl6-end~~
  >>
  
  \new StaffGroup \with{
    \override StaffGrouper.staff-staff-spacing =
    #'((basic-distance . 9) 
        (minimum-distance . 9)
        (padding . 0)
        (stretchability . 0))
  }
  <<
    %{~~hl5~~
    \new Staff \with {
      instrumentName = \markup \center-column {"hl 5" "(high noise)"}
      shortInstrumentName = #"hl5 (hn)  "
    } 
    <<
      \repeat unfold 33 { \repeat unfold 3 { s1 \noBreak } s1 \break }
      \include "includes/a_lot_of_tiles_trivial_scan_hl_5.ly"
    >>
    %}
    
    %{~~hl4~~
    \new Staff \with {
      instrumentName = \markup \center-column {"hl 4 " "(low noise)"}
      shortInstrumentName = #"hl4 (ln)  "
    } 
    <<
      \repeat unfold 33 { \repeat unfold 3 { s1 \noBreak } s1 \break }
      \include "includes/a_lot_of_tiles_trivial_scan_hl_4.ly"
    >>
    %}
  >>
>>